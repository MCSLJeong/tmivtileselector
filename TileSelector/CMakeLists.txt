cmake_minimum_required(VERSION 3.7 FATAL_ERROR)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

add_library(TileSelectorLib
    "src/ITileSelector.cpp"
    "src/TileSelector.cpp"
	"src/TileSelector.reg.hpp"
	"include/TMIV/TileSelector/ITileSelector.h"
	"include/TMIV/TileSelector/TileSelector.h"
	)

add_library(TMIV::TileSelectorLib
	ALIAS TileSelectorLib)

target_link_libraries(TileSelectorLib
	PUBLIC
		TMIV::CommonLib
		TMIV::MetadataLib
		TMIV::ImageLib
		TMIV::RendererLib
		TMIV::IOLib
	PRIVATE
		Threads::Threads
)

target_include_directories(TileSelectorLib
	PUBLIC
		"$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
		"$<INSTALL_INTERFACE:include>")

add_executable(TileSelector
	"src/TileSelector.main.cpp")

target_link_libraries(TileSelector
	PRIVATE
		TMIV::CommonLib
		TMIV::IOLib
		TMIV::TileSelectorLib
)

install(
	TARGETS
		TileSelector
		TileSelectorLib
	EXPORT TileSelector.Targets
	ARCHIVE DESTINATION "lib"
	RUNTIME DESTINATION "bin")

install(
	EXPORT TileSelector.Targets
	FILE "TileSelector.cmake"
	NAMESPACE TMIV::
	DESTINATION "lib/cmake/TMIV")

install(
	DIRECTORY "include/"
	DESTINATION "include")
