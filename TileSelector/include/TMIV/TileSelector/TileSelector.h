/* The copyright in this software is being made available under the BSD
* License, included below. This software may be subject to other third party
* and contributor rights, including patent rights, and no such rights are
* granted under this license.
*
* Copyright (c) 2010-2018, ITU/ISO/IEC
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  * Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
* THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
Original authors:

Universite Libre de Bruxelles, Brussels, Belgium:
  Sarah Fachada, Sarah.Fernandes.Pinto.Fachada@ulb.ac.be
  Daniele Bonatto, Daniele.Bonatto@ulb.ac.be
  Arnaud Schenkel, arnaud.schenkel@ulb.ac.be

Koninklijke Philips N.V., Eindhoven, The Netherlands:
  Bart Kroon, bart.kroon@philips.com
  Bart Sonneveldt, bart.sonneveldt@philips.com
*/

#ifndef _TILESELECTOR_TILESELECTOR_H_
#define _TILESELECTOR_TILESELECTOR_H_

#include <TMIV/Common/Json.h>
#include <TMIV/Common/LinAlg.h>
#include <TMIV/TileSelector/ITileSelector.h>

#define PI		  3.14159265358979323846
#define radperdeg 0.01745329252
#define degperrad 57.295779513

using namespace TMIV::Common;

namespace TMIV::TileSelector
{
namespace TileSelectorMath
{
	template<typename T> T toRadian(T degree);

	template<typename T> T toDegree(T radian);

	Mat3x1f matprod(Mat3x3f A, Mat3x1i B);
    Mat3x1f matprod(Mat3x3f A, Mat3x1f B);
	Mat3x1f matdiv(Mat3x1f A, float b);

	Vec3f matprod(Mat3x3f A, Vec3i B);
    Vec3f matprod(Mat3x3f A, Vec3f B);
	Vec3f matdiv(Vec3f A, float b);
} // namespace TileSelectorMath

class TileSelector : public ITileSelector
{
public:
	// This rotation matrix follows m41993. 
	Vec3f RotateCartesian3D(Vec3f nonRotated, float yaw, float pitch, float roll)
	{
		Vec3f rotated;
		float x = nonRotated[0];
		float y = nonRotated[1];
		float z = nonRotated[2];
		yaw = yaw * radperdeg;
		pitch = pitch * radperdeg;
		roll = roll * radperdeg;

		// 파이썬 코드에서는 x, y, z 거꾸로 입력해야 제대로나옴.
		// 이대로 돌려서 잘 나오면 numpy 문제였던걸로.
		rotated[0] = (cos(roll) * cos(pitch)) * x
			- (sin(roll) * cos(pitch)) * y
			+ sin(pitch) * z;

		rotated[1] = (cos(roll) * sin(pitch) * sin(yaw) + sin(roll) * cos(yaw)) * x
			+ (cos(roll) * cos(yaw) - sin(roll) * sin(pitch) * sin(yaw)) * y
			- (cos(pitch) * cos(yaw)) * z;

		rotated[2] = (sin(roll) * sin(yaw) - cos(roll) * sin(pitch) * cos(yaw)) * x
			+ (cos(roll) * sin(yaw) + sin(roll) * sin(pitch) * cos(yaw)) * y
			+ (cos(pitch) * cos(yaw)) * z;

		return rotated;
	}

	// This rotation matrix follows m41993.
	Mat3x3f computeRotationMatrix(float yaw, float pitch, float roll)
	{
		Mat3x3f rotationMatrix;
		yaw = yaw * radperdeg;
		pitch = pitch * radperdeg;
		roll = roll * radperdeg;
		// 파이썬 코드에서는 x, y, z 거꾸로 입력해야 제대로나옴.
		// 이대로 돌려서 잘 나오면 numpy 문제였던걸로.
		
		/*
		rotationMatrix[0] = cos(roll) * cos(pitch);
		rotationMatrix[1] = - sin(roll) * cos(pitch);
		rotationMatrix[2] = sin(pitch);
		rotationMatrix[3] = cos(roll) * sin(pitch) * sin(yaw) + sin(roll) * cos(yaw);
		rotationMatrix[4] = cos(roll) * cos(yaw) - sin(roll) * sin(pitch) * sin(yaw);
		rotationMatrix[5] = - cos(pitch) * sin(yaw);
		rotationMatrix[6] = sin(roll) * sin(yaw) - cos(roll) * sin(pitch) * cos(yaw);
		rotationMatrix[7] = cos(roll) * sin(yaw) + sin(roll) * sin(pitch) * cos(yaw);
		rotationMatrix[8] = cos(pitch) * cos(yaw);
		*/
		
		
		rotationMatrix[0] = cos(yaw) * cos(pitch);
		rotationMatrix[1] = - sin(yaw) * cos(pitch);
		rotationMatrix[2] = sin(pitch);
		rotationMatrix[3] = cos(yaw) * sin(pitch) * sin(roll) + sin(yaw) * cos(roll);
		rotationMatrix[4] = cos(yaw) * cos(roll) - sin(yaw) * sin(pitch) * sin(roll);
		rotationMatrix[5] = - cos(pitch) * sin(roll);
		rotationMatrix[6] = sin(yaw) * sin(roll) - cos(yaw) * sin(pitch) * cos(roll);
		rotationMatrix[7] = cos(yaw) * sin(roll) + sin(yaw) * sin(pitch) * cos(roll);
		rotationMatrix[8] = cos(pitch) * cos(roll);
		
		return rotationMatrix;
	}
	Mat3x3f rotationMatrixFromRotationAroundX(float rx) {
		return Mat3x3f{1.f, 0.f, 0.f, 0.f, cos(rx), -sin(rx), 0.f, sin(rx), cos(rx)};
	}

	Mat3x3f rotationMatrixFromRotationAroundY(float ry) {
		return Mat3x3f{cos(ry), 0.f, sin(ry), 0.f, 1.f, 0.f, -sin(ry), 0.f, cos(ry)};
	}

	Mat3x3f rotationMatrixFromRotationAroundZ(float rz) {
		return Mat3x3f{cos(rz), -sin(rz), 0.f, sin(rz), cos(rz), 0.f, 0.f, 0.f, 1.f};
	}

	Mat3x3f EulerAnglesToRotationMatrix(Vec3f rotation) {
		return rotationMatrixFromRotationAroundZ(radperdeg * rotation[0]) *
			   rotationMatrixFromRotationAroundY(radperdeg * rotation[1]) *
			   rotationMatrixFromRotationAroundX(radperdeg * rotation[2]);
	}

	double computeL2Norm(Mat3x1f p)
	{
		double result = 0;

		for (double x : p)
		{
			result += x * x;
		}

		std::cout << "L2 Norm: " << sqrt(result) << std::endl;
		return sqrt(result);
	}

	double computeL2Norm(Vec3f p)
	{
		double result = 0;

		for (double x : p)
		{
			result += x * x;
		}

		//std::cout << "L2 Norm: " << sqrt(result) << std::endl;
		return sqrt(result);
	}

	Vec3f sphericalToCartesian3D(Vec2f s)
	{
		Vec3f c;

		float phi = s[0];
		float theta = s[1];
		c[0] = cos(phi) * cos(theta);
		c[1] = sin(phi) * cos(theta);
		c[2] = sin(theta);

		return c;
	}

	Vec2f cartesian3DToSpherical(Vec3f c)
	{
		Vec2f s;
		s[0] = TileSelectorMath::toDegree(atan2(c[1], c[0]));
		s[1] = TileSelectorMath::toDegree(asin(round(c[2] * 1000000000) / 1000000000));

		return s;
	}

	Vec2i sphericalToERP(Vec2f s, int width, int height)
	{
		Vec2i e;
		float phi = s[0];
		float theta = s[1];
		e[0] = int(width * (0.5f - (phi / 360.0f)));
		e[1] = int(height * (0.5f - (theta / 180.0f)));

		return e;
	}
private:
	int m_rows;
	int m_columns;
	
	// Viewport 2D homogeneous points. {x, y, 1}
	//std::vector<Mat3x1i>		m_viewportCart2DPoints;
	std::vector<Vec3i>		m_viewportCart2DPoints;

	// Rotated 3D world coordinates. {x, y, z}
	//std::vector<Vec3f>		m_viewportRotatedCart3DPoints;

	// Rotated ERP coordinates. {x, y}
	std::vector<Vec2i>		 m_viewportRotatedERPPoints;

	// Viewport points on source views.
	std::vector<Vec2i>		 m_viewportPointsOnSourceViews;

	int m_width;
	int m_height;

	int m_viewNumTileRows;
	int m_viewNumTileColumns;

	int m_viewTileWdt;
	int m_viewTileHgt;

	// To reduce the complexity, a part of viewport 2D points are inserted into m_viewportCartPoints.
	// If m_viewportPointInterval == 50, points (0, 0), (0, 50), (0, 100), ... are inserted
	int m_viewportCartPointInterval = 50;

	enum heuristicMode
	{
		FALSE, 
		FIVE_POINT_MODE,
	};
	heuristicMode m_heuristicMode = FALSE;

	// In MPEG/N18464: erp_phi_{min,max}
	Vec2f m_erpPhiRange{};

	// In MPEG/N18464: erp_theta_{min,max}
	Vec2f m_erpThetaRange{};

	// In MPEG/N18464: cam_{yaw,pitch,roll}
	Vec3f m_viewportRotation{0, 0, 0};

	Mat3x3f m_viewportRotationMatrix;
	
	// If the view is omnidirectional ERP, m_viewRangeHor = 360.0, m_viewRangeVer = 180.0
	// m_viewRangeHor = m_erpPhiRange[1] - m_erpPhiRange[0]
	float m_viewRangeHor = 360.0;
	float m_viewRangeVer = 180.0;

	// FoV range in degree. Default value is 90.0
	float m_viewportRangeHor = 90.0;
	float m_viewportRangeVer = 90.0;

	// Matrix K.
	Mat3x3f m_matK;
	
	// Inverse matrix of K.
	Mat3x3f m_matInvK;
	
	Vec2i							m_viewSize = { 4096, 2048 };
	Vec2i							m_viewNumTile = {4, 8};
	Vec2i							m_viewTileSize = {512, 512};
	std::vector<std::vector<int>>	m_viewTileIdxList;
	Vec2i							m_viewportSize = { 2048, 2048 };
	Vec2f							m_viewRange = { 360.0, 180.0 };
	Vec2f							m_viewportRange = { 90.0, 90.0 };
	Metadata::CameraParametersList	m_cameras;
	Metadata::CameraParameters		m_viewportCamera;
	Metadata::CameraParameters		m_nonRotatedCamera;
	TextureDepth16Frame				m_viewportBuffer;
	MVD16Frame						m_viewBuffer;


public:
	TileSelector(const Common::Json & /*rootNode*/,
		const Common::Json & /*componentNode*/);
	TileSelector(const TileSelector &) = delete;
	TileSelector(TileSelector &&) = default;
	TileSelector &operator=(const TileSelector &) = delete;
	TileSelector &operator=(TileSelector &&) = default;
	~TileSelector() override = default;

	using Vec2i = Common::Vec2i;

	void prepareByFrame(CameraParametersList cameras, CameraParameters target) override;
	void pushFrame(MVD16Frame views) override;
	void completeByFrame() override;
	void computeViewport() override;
	void computeViewportERPCoordinate() override;
	void computeViewportFromSourceViews() override;
	int	 computeTileIdx(Vec2i point);
	void saveViewportMarkedTexture(const Json &config, int frameIndex) override;

	const CameraParametersList &getCameraList() const override {
		return m_cameras;
	}

	const CameraParameters &getTargetCamera() const override {
		return m_viewportCamera;
	}

	void pushViewportCartPoints()
	{
		int width = m_viewportSize[0];
		int height = m_viewportSize[1];
		for (int row = 0; row < width; row += m_viewportCartPointInterval)
		{
			for (int col = 0; col < height; col += m_viewportCartPointInterval)
			{
				//m_viewportCart2DPoints.push_back(Mat3x1i{ row, col, 1 });
				m_viewportCart2DPoints.push_back(Vec3i{ row, col, 1 });
			}
		}
	}

	void setViewSize()
	{
		m_width = m_viewSize[0];
		m_height = m_viewSize[1];
	}

	void setViewNumTile()
	{
		m_viewNumTileRows = m_viewNumTile[0];
		m_viewNumTileColumns = m_viewNumTile[1];
		std::cout << "m_viewNumTileRows: " << m_viewNumTileRows << std::endl;
		std::cout << "m_viewNumTileColumns: " << m_viewNumTileColumns << std::endl;
	}

	void setViewTileSize()
	{
		m_viewTileWdt = m_viewTileSize[0];
		m_viewTileHgt = m_viewTileSize[1];
		std::cout << "m_viewTileSize: " << m_viewTileSize << std::endl;
	}

	void setViewRange()
	{
		m_viewRangeHor = m_viewRange[0];
		m_viewRangeVer = m_viewRange[1];
	}

	void setViewportRange()
	{
		m_viewportRangeHor = m_viewportRange[0];
		m_viewportRangeVer = m_viewportRange[1];
	}

	void matInv()
	{
		float det = m_matK[0] * m_matK[4];
		m_matInvK[0] = (m_matK[4] * m_matK[8] - m_matK[7] * m_matK[5]) / det;
		m_matInvK[1] = (m_matK[2] * m_matK[7] - m_matK[1] * m_matK[8]) / det;
		m_matInvK[2] = (m_matK[1] * m_matK[5] - m_matK[2] * m_matK[4]) / det;
		m_matInvK[3] = (m_matK[5] * m_matK[6] - m_matK[3] * m_matK[8]) / det;
		m_matInvK[4] = (m_matK[0] * m_matK[8] - m_matK[2] * m_matK[6]) / det;
		m_matInvK[5] = (m_matK[3] * m_matK[2] - m_matK[0] * m_matK[5]) / det;
		m_matInvK[6] = (m_matK[3] * m_matK[7] - m_matK[6] * m_matK[4]) / det;
		m_matInvK[7] = (m_matK[6] * m_matK[1] - m_matK[0] * m_matK[7]) / det;
		m_matInvK[8] = (m_matK[0] * m_matK[4] - m_matK[3] * m_matK[1]) / det;
		//std::cout << "m_matInvK: " << m_matInvK << std::endl;
	}

	// Referenced TViewPort.cpp line 95 from 360lib 5.1.
	void setInvK()
	{
		float fovx = PI * (m_viewportRangeHor / 180.0);
		float fovy = PI * (m_viewportRangeVer / 180.0);

		float fx = (m_viewportSize[0] / 2) * (1 / tan(fovx / 2));
		float fy = (m_viewportSize[1] / 2) * (1 / tan(fovy / 2));

		m_matK = Mat3x3f{fx, 0, m_viewportSize[0] / 2.f, 
						0, fy, m_viewportSize[1] / 2.f, 
						0, 0, 1};
		//std::cout << "m_matK: " << std::endl;
		//std::cout << m_matK << std::endl;
		matInv();
		//std::cout << "m_matInvK: " << std::endl;
		//std::cout << m_matInvK << std::endl;
	}
};
} // namespace TileSelector

#endif