#ifndef _TILESELECTOR_ITILESELECTOR_H_
#define _TILESELECTOR_ITILESELECTOR_H_

#include <TMIV/Common/Frame.h>
#include <TMIV/Metadata/CameraParametersList.h>
#include <TMIV/IO/IO.h>

namespace TMIV::TileSelector
{
class ITileSelector
{
public:
	ITileSelector() = default;
	ITileSelector(const ITileSelector &) = delete;
	ITileSelector(ITileSelector &&) = default;
	ITileSelector &operator=(const ITileSelector &) = delete;
	ITileSelector &operator=(ITileSelector &&) = default;
	virtual ~ITileSelector() = default;

	using MVD16Frame = TMIV::Common::MVD16Frame;
	using CameraParametersList = Metadata::CameraParametersList;
	using CameraParameters = Metadata::CameraParameters;

	virtual void prepareByFrame(CameraParametersList cameras, CameraParameters target) = 0;
	virtual void pushFrame(MVD16Frame views) = 0;
	virtual void completeByFrame() = 0;
	virtual void computeViewport() = 0;
	virtual void computeViewportERPCoordinate() = 0;
	virtual void computeViewportFromSourceViews() = 0;
	virtual void saveViewportMarkedTexture(const Common::Json &config, int frameIndex) = 0;

	virtual const CameraParametersList &getCameraList() const = 0;
	virtual const CameraParameters &getTargetCamera() const = 0;
};
}

#endif