/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2019, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <TMIV/Common/Application.h>
#include <TMIV/Common/Factory.h>
#include <TMIV/Common/Thread.h>
#include <TMIV/IO/IO.h>
#include <TMIV/TileSelector/ITileSelector.h>

#include <iostream>
#include <stdexcept>
#include <string>

using namespace std;
using namespace TMIV::Common;
using namespace TMIV::Metadata;

namespace TMIV::TileSelector {

class Application : public Common::Application {
private:
  unique_ptr<ITileSelector> m_tileSelector;
  int m_numberOfFrames;
  int m_intraPeriod;
  CameraParametersList m_cameras;

public:
  explicit Application(vector<const char *> argv)
      : Common::Application{"TileSelector", move(argv)},
        m_tileSelector{create<ITileSelector>("TileSelector")},
        m_numberOfFrames{json().require("numberOfFrames").asInt()},
        m_intraPeriod{json().require("intraPeriod").asInt()} {}

  void run() override {
    m_cameras = IO::loadSourceMetadata(json());

    for (int i = 0; i < m_numberOfFrames; i ++) {
      std::cout << "Frame " << i << std::endl;
      runByFrame(i);
    }
  }

private:
  void runByFrame(int frame) {
    auto cameras = IO::loadSourceMetadata(json());
    auto target = IO::loadViewportMetadata(json(), frame);
    auto srcFrame = IO::loadSourceFrame(json(), IO::sizesOf(m_cameras), frame);
    m_tileSelector->prepareByFrame(cameras, target);
    m_tileSelector->pushFrame(move(srcFrame));
    m_tileSelector->completeByFrame();
    m_tileSelector->saveViewportMarkedTexture(json(), frame);
  }
};
} // namespace TileSelector

#include "TileSelector.reg.hpp"

int main(int argc, char *argv[]) {
  try {
    TMIV::TileSelector::registerComponents();
    TMIV::TileSelector::Application app{{argv, argv + argc}};
    app.run();
    return 0;
  } catch (runtime_error &e) {
    cerr << e.what() << endl;
  }
}
