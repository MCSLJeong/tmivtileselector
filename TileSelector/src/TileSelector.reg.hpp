#include <TMIV/TileSelector/TileSelector.h>
#include <TMIV/Common/Factory.h>

namespace TMIV::TileSelector
{
inline void registerComponents()
{
	Common::Factory<ITileSelector>::getInstance().registerAs<TileSelector>("TileSelector");
}
}
