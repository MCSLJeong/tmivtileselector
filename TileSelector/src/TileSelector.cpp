/* The copyright in this software is being made available under the BSD
* License, included below. This software may be subject to other third party
* and contributor rights, including patent rights, and no such rights are
* granted under this license.
*
* Copyright (c) 2010-2018, ITU/ISO/IEC
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  * Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
*    be used to endorse or promote products derived from this software without
*    specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
* THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
Original authors:

Universite Libre de Bruxelles, Brussels, Belgium:
  Sarah Fachada, Sarah.Fernandes.Pinto.Fachada@ulb.ac.be
  Daniele Bonatto, Daniele.Bonatto@ulb.ac.be
  Arnaud Schenkel, arnaud.schenkel@ulb.ac.be

Koninklijke Philips N.V., Eindhoven, The Netherlands:
  Bart Kroon, bart.kroon@philips.com
  Bart Sonneveldt, bart.sonneveldt@philips.com
*/

#include <iostream>
#include <TMIV/Image/Image.h>
#include <TMIV/Renderer/reprojectPoints.h>
#include <TMIV/TileSelector/TileSelector.h>
#include <TMIV/Common/Factory.h>

using namespace std;
using namespace TMIV::Common;
using namespace TMIV::Image;
using namespace TMIV::Renderer;

namespace TMIV::TileSelector
{
namespace TileSelectorMath {
template <typename T> T toRadian(T degree) { return degree * radperdeg; }

template <typename T> T toDegree(T radian) { return radian * degperrad; }

Mat3x1f matprod(Mat3x3f A, Mat3x1i B) {
  Mat3x1f result = {0.0, 0.0, 0.0};
  for (int row = 0; row < 3; row++) {
    for (int col = 0; col < 3; col++) {
      result[row] += A[row * 3 + col] * float(B[col]);
    }
  }
  return result;
}
Mat3x1f matprod(Mat3x3f A, Mat3x1f B) {
  Mat3x1f result = {0.0, 0.0, 0.0};
  for (int row = 0; row < 3; row++) {
    for (int col = 0; col < 3; col++) {
      result[row] += A[row * 3 + col] * B[col];
    }
  }
  return result;
}
Mat3x1f matdiv(Mat3x1f A, float b) {
  Mat3x1f result = Mat3x1f{0.0, 0.0, 0.0};
  for (int i = 0; i < 3; i++) {
    result[i] = A[i] / b;
  }
  return result;
}
Vec3f matprod(Mat3x3f A, Vec3i B) {
  Vec3f result = {0.0, 0.0, 0.0};
  for (int row = 0; row < 3; row++) {
    for (int col = 0; col < 3; col++) {
      result[row] += A[row * 3 + col] * float(B[col]);
    }
  }
  return result;
}
Vec3f matprod(Mat3x3f A, Vec3f B) {
  Vec3f result = {0.0, 0.0, 0.0};
  for (int row = 0; row < 3; row++) {
    for (int col = 0; col < 3; col++) {
      result[row] += A[row * 3 + col] * B[col];
    }
  }
  return result;
}
Vec3f matdiv(Vec3f A, float b) {
  Vec3f result = Vec3f{0.0, 0.0, 0.0};
  for (int i = 0; i < 3; i++) {
    result[i] = A[i] / b;
  }
  return result;
}
} // namespace TileSelectorMath

TileSelector::TileSelector(const Common::Json &rootNode,
	const Common::Json &componentNode)
{
	if (auto subnode = componentNode.optional("ViewResolution")) {
		m_viewSize = subnode.asIntVector<2>();
		setViewSize();
	}

	if (auto subnode = componentNode.optional("ViewNumTile")) {
		m_viewNumTile = subnode.asIntVector<2>();
		setViewNumTile();
	}

	if (auto subnode = componentNode.optional("ViewTileSize")) {
		m_viewTileSize = subnode.asIntVector<2>();
		setViewTileSize();
	}

	if (auto subnode = componentNode.optional("ViewRange")) {
		m_viewRange = subnode.asFloatVector<2>();
		setViewRange();
	}

	if (auto subnode = componentNode.optional("ViewportRange")) {
		m_viewportRange = subnode.asFloatVector<2>();
		setViewportRange();
	}

	if (auto subnode = componentNode.optional("ViewportPointInterval")) {
		m_viewportCartPointInterval = subnode.asFloat();
	}
	if (auto subnode = componentNode.optional("HeuristicMode")) {
		string strHeuristicMode = subnode.asString();
		if (strHeuristicMode.compare("FIVE_POINT_MODE"))
		{
			m_heuristicMode = FIVE_POINT_MODE;
		}
	}
	//m_nonRotatedCamera.position = { 0, 0, 0 };
	//m_nonRotatedCamera.rotation = { 0, 0, 0 };
}

void TileSelector::prepareByFrame(CameraParametersList cameras, CameraParameters target)
{
	m_cameras.clear();
	m_cameras.insert(m_cameras.end(), make_move_iterator(begin(cameras)), make_move_iterator(end(cameras)));
	m_viewportCamera = target;
	m_viewportRotation = target.rotation;
	std::cout << "m_viewportCamera: " << m_viewportCamera << std::endl;
	m_viewportCart2DPoints.clear();
	m_viewportRotatedERPPoints.clear();
	std::fill(m_viewportBuffer.first.getPlanes().begin(), m_viewportBuffer.first.getPlanes().end(), uint16_t(0));
	std::fill(m_viewportBuffer.second.getPlanes().begin(), m_viewportBuffer.second.getPlanes().end(), uint16_t(0));
	m_viewBuffer.clear();
	setInvK();
	// 20210603
    m_viewTileIdxList.clear();
}

void TileSelector::completeByFrame()
{
	//printf("computeViewport begin\n");
	computeViewport();
    //printf("computeViewport end\n");
	//printf("computeViewportFromSourceViews begin\n");
	computeViewportFromSourceViews();
	//printf("computeViewportFromSourceViews end\n");
}

void TileSelector::computeViewport()
{
	computeViewportERPCoordinate();
}

void TileSelector::computeViewportERPCoordinate()
{
	// Push viewport 2D points.
	pushViewportCartPoints();

	// Compute rotation matrix of viewport.
	//std::cout << "m_viewportRotation: " << std::endl;
	//std::cout << m_viewportRotation << std::endl;

	// TMIV renderer가 viewport를 뽑는대로 하려면 rotation matrix도 TMIV 식으로 뽑아야할거같음.
	//m_viewportRotationMatrix = computeRotationMatrix(m_viewportRotation[0], m_viewportRotation[1], m_viewportRotation[2]);
	m_viewportRotationMatrix = computeRotationMatrix(0, 0, 0);
	//m_viewportRotationMatrix = EulerAnglesToRotationMatrix(m_viewportRotation);
	std::cout << "m_viewportRotationMatrix: " << std::endl;
	std::cout << m_viewportRotationMatrix << std::endl;
	//std::cout << "m_viewportCart2DPoints.size(): " << m_viewportCart2DPoints.size() << std::endl;
	// Convert CartPoints to ERPPoints.
	for (int p = 0; p < m_viewportCart2DPoints.size(); p++)
	{
		// Convert Cart2Dpoints to Cart3Dpoints.
        auto viewportCart2DPoints = m_viewportCart2DPoints.at(p);
		//std::cout << "m_viewportCart2DPoints.at(p): " << m_viewportCart2DPoints.at(p) << std::endl;
		//auto matrixE = m_viewportRotationMatrix * ((m_matInvK * viewportCart2DPoints) / (computeL2Norm(m_matInvK * viewportCart2DPoints)));
        //auto matrixE = TMIV::TileSelector::TileSelectorMath::matprod(m_matInvK, viewportCart2DPoints);
        //matrixE = TMIV::TileSelector::TileSelectorMath::matprod(m_viewportRotationMatrix, matrixE);
        //matrixE = TMIV::TileSelector::TileSelectorMath::matdiv(matrixE, (computeL2Norm(TMIV::TileSelector::TileSelectorMath::matprod(m_matInvK, viewportCart2DPoints))));
        auto matrixE = m_matInvK * (Vec3f)viewportCart2DPoints;
        matrixE = m_viewportRotationMatrix * matrixE;
        matrixE = matrixE / (computeL2Norm(m_matInvK * (Vec3f)viewportCart2DPoints));
		// 파이썬 코드에서는 x, y, z 거꾸로 입력해야 제대로나옴.
		// 이대로 돌려서 잘 나오면 numpy 문제였던걸로.
		auto rotatedCart3Dx = matrixE[2];
		auto rotatedCart3Dy = matrixE[1];
		auto rotatedCart3Dz = matrixE[0];
		//auto rotatedCart3Dx = matrixE[0];
		//auto rotatedCart3Dy = matrixE[1];
		//auto rotatedCart3Dz = matrixE[2];
		//std::cout << "rotatedCart3Dx: " << rotatedCart3Dx << std::endl;
		//std::cout << "rotatedCart3Dy: " << rotatedCart3Dy << std::endl;
		//std::cout << "rotatedCart3Dz: " << rotatedCart3Dz << std::endl;

		// Convert Cart3Dpoints to SphrPoints.
		float phi = degperrad * atan2(rotatedCart3Dy, rotatedCart3Dx);
		float theta = degperrad * asin(rotatedCart3Dz);

		// Convert SphrPoints to ERPPoints.
		//m_viewportRotatedERPPoints.push_back(Vec2i{ int(m_width * (0.5 - phi / 360.0)), int(m_height * (0.5 - theta / 180.0)) });
		//m_viewportRotatedERPPoints.push_back(Vec2i{ int(m_width * (0.5 + phi / 360.0)), int(m_height * (0.5 + theta / 180.0)) });
		int left_x = m_width * ((180.0 - m_viewRangeHor / 2) / 360.0);
		int right_x = m_width * ((180.0 + m_viewRangeHor / 2) / 360.0);
		int top_y = m_height * ((90.0 - m_viewRangeVer / 2) / 180.0);
		int bottom_y = m_height * ((90.0 + m_viewRangeVer / 2) / 180.0);

		if ((int(m_width * (0.5 + phi / 360.0)) > (left_x - 1))
			&& (int(m_width * (0.5 + phi / 360.0)) < right_x)
			&& (int(m_height * (0.5 + theta / 180.0)) > (top_y - 1))
			&& (int(m_height * (0.5 + theta / 180.0)) < (bottom_y)))
		{
			m_viewportRotatedERPPoints.push_back(Vec2i{ int(int(m_width * (0.5 + phi / 360.0)) - left_x), int(int(m_height * (0.5 + theta / 180.0)) - top_y)});
                  /*printf("int(m_width * (0.5 + phi / 360.0)) - left_x: %d, "
                         "int(m_height * (0.5 + theta / 180.0)) - top_y:%d\n",
                         int(m_width * (0.5 + phi / 360.0)) - left_x,
                         int(m_height * (0.5 + theta / 180.0)) - top_y);*/
		}
		//std::cout << "left_x: " << left_x << ", right_x: " << right_x << std::endl;
		//m_viewportRotatedERPPoints.push_back(Vec2i{ int(m_width * (0.5 + phi / 360.0)) - 1024, int(m_height * (0.5 + theta / 180.0)) });
		//std::cout << "int(m_width * (0.5 - phi / 360.0)), int(m_height * (0.5 - theta / 180.0)): " << int(m_width * (0.5 - phi / 360.0)) << " " << int(m_height * (0.5 - theta / 180.0)) << std::endl;	
	}
	std::cout << "m_viewportRotatedERPPoints.size(): " << m_viewportRotatedERPPoints.size() << std::endl;
}

void TileSelector::computeViewportFromSourceViews()
{
	//printf("m_viewportRotatedERPPoints.size: %d\n", m_viewportRotatedERPPoints.size());
	// 20210603
	// I found that tiles outside the viewport were included at the end of the frame index. 
	// They were viewport tiles at the first time. 
	// In short, m_viewTileIdxList needs to be cleared for every frame. 
	// This will be done in prepareByFrame(); 
	for (int viewId = 0; viewId < m_cameras.size(); viewId++)
	{
		//printf("viewId: %d\n", viewId);
		m_viewportPointsOnSourceViews.clear();
		std::vector<int> viewTileList;
		m_viewTileIdxList.push_back(viewTileList);

		const Mat<float> &depthMapExpanded = expandDepth(m_cameras[viewId], m_viewBuffer[viewId].second);
		Mat<Vec2f> imagePosSrc = imagePositions(m_cameras[viewId]);

		//std::cout << "m_cameras[" << viewId << "]: " << m_cameras[viewId] << std::endl;
		int wdt = m_cameras[viewId].size[0];
		int hgt = m_cameras[viewId].size[1];

		// Find viewport points on source views.
		for (int row = 0; row < hgt; row++)
		{
			for (int col = 0; col < wdt; col++)
			{
				// Warping from each source view point to the viewport.
				auto ptsSrcToViewport = reprojectAPoint(m_cameras[viewId], m_viewportCamera,
					imagePosSrc(row, col), depthMapExpanded(row, col));
				float zSrcToViewport = ptsSrcToViewport.second;
				//if (!std::isnan(zSrcToViewport))
				if (1)
				{
					int x1 = int(floor(ptsSrcToViewport.first.x()));
					int y1 = int(floor(ptsSrcToViewport.first.y()));
					int tileIdx = computeTileIdx(Vec2i{col, row});
					auto res = std::find(
                            m_viewTileIdxList[viewId].begin(),
                            m_viewTileIdxList[viewId].end(), tileIdx);

					// If the tile is not included, compute
					// Else, push the tile index and skip
					//if (res == m_viewTileIdxList[viewId].end())
					if (1)
					{
						// If the warped point is same as the viewport points, 
						// Push it to m_viewportPointsOnSourceViews. 
						for (auto p : m_viewportRotatedERPPoints)
						{	
							if ((p.x() == x1) && (p.y() == y1))
							{
								m_viewportPointsOnSourceViews.push_back(Vec2i{col, row});
								if (res == m_viewTileIdxList[viewId].end())
									m_viewTileIdxList[viewId].push_back(tileIdx);
								break;
							}
						}
					}
				}
			}
		}
		int Ysize = wdt * hgt;
		int UVsize = int(Ysize / 4);
		//printf("m_viewportPointsOnSourceViews.size: %d\n", m_viewportPointsOnSourceViews.size());
		
		printf("view %d tileIdx: ", viewId);
		std::sort(m_viewTileIdxList[viewId].begin(), m_viewTileIdxList[viewId].end());
		for (auto tileIdx : m_viewTileIdxList[viewId])
		{
			printf("%d ", tileIdx);
		}
		printf("\n");

		// Draw tile lines and mark tiles
		auto &texture = m_viewBuffer[viewId].first;
		for (int row = 0; row < m_viewNumTileRows; row++)
		{
			for (int c = 0; c < wdt; c++)
			{
				//auto &texture = m_viewBuffer[viewId].first.getPlanes();
				// Y
				texture.getPlane(0)[row * m_viewTileHgt * wdt + c] = 0;
				// UV
				int UVindex = ((row * m_viewTileHgt) / 2) * (wdt / 2) + (c / 2);
				//texture.getPlane(1)[row * m_viewTileHgt * wdt + c] = 0;
				//texture.getPlane(2)[Ysize + UVsize + UVindex] = 0;
				texture.getPlane(1)[UVindex] = 512;
				texture.getPlane(2)[UVindex] = 512;
			}
		}
		//printf("Drawing horizontal lines end\n");
		for (int col = 0; col < m_viewNumTileColumns; col++)
		{
			for (int r = 0; r < hgt; r++)
			{
				//auto &texture = m_viewBuffer[viewId].first.getPlane(0);
				// Y
				texture.getPlane(0)[r * wdt + col * m_viewTileWdt] = 0;
				// UV
				int UVindex = (r / 2) * (wdt / 2) + (col * m_viewTileWdt / 2);
				texture.getPlane(1)[UVindex] = 512;
				texture.getPlane(2)[UVindex] = 512;
			}
		}
		//printf("Drawing lines end\n");
		for (auto tileIdx : m_viewTileIdxList[viewId])
		{
			int leftX = int(tileIdx % m_viewNumTileColumns) * m_viewTileWdt;
			int leftY = int(tileIdx / m_viewNumTileColumns) * m_viewTileHgt;

			for (int row = leftY; row < leftY + m_viewTileHgt; row++)
			{
				for (int col = leftX; col < leftX + m_viewTileWdt; col++)
				{
					//auto &texture = m_viewBuffer[viewId].first.getPlane(0);
					int UVindex = (row / 2) * (wdt / 2) + (col / 2);
					texture.getPlane(1)[UVindex] = 0;
					texture.getPlane(2)[UVindex] = 0;
				}
			}
		}

		for (auto point : m_viewportPointsOnSourceViews)
		{
			int x = point.x();
			int y = point.y();
			texture.getPlane(0)[y * wdt + x] = 0;

			int UVindex = (y / 2) * (wdt / 2) + (x / 2);
			texture.getPlane(1)[UVindex] = 512;
			texture.getPlane(2)[UVindex] = 512;
		}

		// Draw viewport points on the source views.
		/*
		for (auto p : m_viewportPointsOnSourceViews)
		{
			//std::cout << "p: " << p << std::endl;
			//auto &texture = m_viewBuffer[viewId].first.getPlanes();
			auto &texture = m_viewBuffer[viewId].first.getPlane(0);
			// Y
			//std::cout << "p[0] + p[1] * wdt: " << p[0] + p[1] * wdt << std::endl;
			texture[p[0] + p[1] * wdt] = 0;
			// UV
			int UVindex = (p[1] / 2) * (wdt / 2) + (p[0] / 2);
			for (int layer = 1; layer < 3; layer++)
			{
				//std::cout << "Ysize + UVindex: " << Ysize + UVindex << std::endl;
				texture[Ysize + UVindex] = 0;
				//std::cout << "Ysize + UVsize + UVindex: " << Ysize + UVsize + UVindex << std::endl;
				texture[Ysize + UVsize + UVindex] = 0;
			}
		}
		*/
		//std::cout << "Drawing viewport points end" << std::endl;
	}
}

int TileSelector::computeTileIdx(Vec2i point)
{
	int tileIdxInRow = int(point.y() / m_viewTileHgt);
	int tileIdxInCol = int(point.x() / m_viewTileWdt);

	int tileIdx = (tileIdxInRow * m_viewNumTileColumns) + tileIdxInCol;

	return tileIdx;
}

void TileSelector::saveViewportMarkedTexture(const Json &config, int frameIndex)
{
	TMIV::IO::saveViewportMarkedFrame(config,frameIndex, m_viewBuffer);
}

void TileSelector::pushFrame(MVD16Frame views)
{
	m_viewBuffer.insert(m_viewBuffer.end(), make_move_iterator(begin(views)),
		make_move_iterator(end(views)));
}
}